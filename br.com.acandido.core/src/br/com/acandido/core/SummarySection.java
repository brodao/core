package br.com.acandido.core;

import java.io.PrintWriter;
import java.util.Enumeration;

import org.eclipse.ui.about.ISystemSummarySection;
import org.osgi.framework.Bundle;
import org.osgi.framework.Version;

public class SummarySection implements ISystemSummarySection {

	@Override
	public void write(final PrintWriter writer) {
		Version version = CoreActivator.getContext().getBundle().getVersion();
		String[] sb = ACUtils.getDeveloperInfo(version).toString().split("\\n");

		for (String line : sb) {
			writer.println(line);
		}

		Bundle[] bundles = CoreActivator.getContext().getBundles();
		for (Bundle bundle : bundles) {
			if (bundle.getSymbolicName().startsWith("br.com.acandido")) {
				writer.print("id: ");
				writer.print(bundle.getSymbolicName());
				writer.print(" vers�o: ");
				writer.println(bundle.getVersion().toString());
				writer.print("\t");
				writer.println(bundle.getLocation());

				Enumeration<String> keys = bundle.getHeaders().keys();
				while (keys.hasMoreElements()) {
					String key = keys.nextElement();
					writer.println("\t" + key + " \t" + bundle.getHeaders().get(key));
				}
			}
		}

		writer.println("");
	}
}
