package br.com.acandido.core;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.ui.statushandlers.StatusManager.INotificationListener;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import br.com.acandido.core.logger.ACLogger;
import br.com.acandido.core.logger.CoreLoggerListener;
import br.com.acandido.core.status.ACCancelStatus;
import br.com.acandido.core.status.ACErrorStatus;
import br.com.acandido.core.status.ACInfoStatus;
import br.com.acandido.core.status.ACWarningStatus;

public class CoreActivator implements BundleActivator {

	public static final String PLUGIN_ID = "br.com.acandido.core";
	private static BundleContext context;

	/**
	 * @return contexto de execução do adicional.
	 */
	public static BundleContext getContext() {
		return context;
	}

	private ILog logger;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(final BundleContext bundleContext) throws Exception {
		CoreActivator.context = bundleContext;

		INotificationListener listener = new CoreLoggerListener();
		StatusManager.getManager().addListener(listener);

		logger = Platform.getLog(bundleContext.getBundle());
		ACLogger.configureLogger(CoreActivator.PLUGIN_ID, logger);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(final BundleContext bundleContext) throws Exception {
		ACLogger.unconfigureLogger(CoreActivator.PLUGIN_ID);

		CoreActivator.context = null;
	}

	/**
	 * Notifica a ocorrência de erro.
	 * 
	 * @param throwable
	 */
	public static void error(Throwable throwable) {
		ACErrorStatus status = new ACErrorStatus(CoreActivator.PLUGIN_ID, throwable);
		StatusManager.getManager().handle(status, StatusManager.BLOCK);
	}

	/**
	 * Notifica a ocorrência de aviso.
	 * 
	 * @param warning
	 * @param args
	 */
	public static void warning(String warning, Object... args) {
		ACWarningStatus status = new ACWarningStatus(CoreActivator.PLUGIN_ID, warning, args);
		StatusManager.getManager().handle(status, StatusManager.SHOW);
	}

	/**
	 * Notifica a ocorrência de informação.
	 * 
	 * @param information
	 * @param args
	 */
	public static void info(String information, Object... args) {
		ACInfoStatus status = new ACInfoStatus(CoreActivator.PLUGIN_ID, information, args);
		StatusManager.getManager().handle(status, StatusManager.SHOW);
	}

	/**
	 * Notifica um cancelamento.
	 * 
	 * @param reason
	 * @param args
	 */
	public static void cancel(String reason, Object... args) {
		ACCancelStatus status = new ACCancelStatus(CoreActivator.PLUGIN_ID, reason, args);
		StatusManager.getManager().handle(status, StatusManager.BLOCK);
	}

	/**
	 * Notifica um erro.
	 * 
	 * @param error
	 * @param args
	 */
	public static void error(String error, Object... args) {
		ACErrorStatus status = new ACErrorStatus(CoreActivator.PLUGIN_ID, error, args);
		StatusManager.getManager().handle(status, StatusManager.BLOCK);
	}

	/**
	 * Log um ocorrência.
	 * 
	 * @param message
	 * @param args
	 */
	public static void log(String message, Object... args) {
		ACInfoStatus status = new ACInfoStatus(CoreActivator.PLUGIN_ID, message, args);
		StatusManager.getManager().handle(status, StatusManager.SHOW);
	}
}
