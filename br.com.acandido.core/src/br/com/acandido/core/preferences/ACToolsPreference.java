package br.com.acandido.core.preferences;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.IWorkbenchPropertyPage;
import org.eclipse.wb.swt.ResourceManager;

/**
 * Prefer�ncias do Developer Studio.
 * 
 * @author Alan C�ndido <brodao@gmail.com>
 *
 */
public class ACToolsPreference extends PreferencePage implements IWorkbenchPreferencePage, IWorkbenchPropertyPage {

	// private static final String URL_OFICIAL =
	// "http://actools.sourceforge.net/history.html";
	private static final String URL_LOCAL = "http://actools.sourceforge.net/history.html";
	private static final String URL = URL_LOCAL;

	/**
	 * Construtor.
	 */
	public ACToolsPreference() {
		this("AC Ferramentas", ResourceManager.getPluginImageDescriptor("br.com.acandido.core", "icon/icone_16x16.png"));
	}

	/**
	 * Construtor.
	 * 
	 * @param title
	 *          T�tulo da p�gina.
	 * @wbp.parser.constructor
	 */
	public ACToolsPreference(final String title) {
		this(title, ResourceManager.getPluginImageDescriptor("br.com.acandido.core", "icon/icone_64x64.png"));
	}

	/**
	 * Construtor.
	 * 
	 * @param title
	 *          T�tulo da p�gina.
	 * @param image
	 *          �cone.
	 */
	public ACToolsPreference(final String title, final ImageDescriptor image) {
		super(title, image);

		setDescription("Informe-se sobre as \u00FAltimas atualiza\u00E7\u00F5es.");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(final IWorkbench workbench) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.
	 * swt.widgets.Composite)
	 */
	@Override
	protected Control createContents(final Composite parent) {
		Composite top = new Composite(parent, SWT.LEFT);
		top.setLayout(new GridLayout(1, false));

		Browser browser = new Browser(top, SWT.NONE);
		browser.setUrl(URL);
		browser.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		return top;
	}

	@Override
	public IAdaptable getElement() {
		return null;
	}

	@Override
	public void setElement(final IAdaptable element) {

	}

}
