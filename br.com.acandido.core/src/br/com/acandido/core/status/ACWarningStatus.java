package br.com.acandido.core.status;

import org.eclipse.core.runtime.IStatus;

/**
 * @author Alan C�ndido <brodao@gmail.com>
 *
 */
public class ACWarningStatus extends ACStatus {

	/**
	 * Gera status de informa��o.
	 * 
	 * @param pluginId
	 * @param warning
	 */
	public ACWarningStatus(String pluginId, String warning, Object... args) {
		super(pluginId, IStatus.WARNING, 0, format(warning, args), null);
	}

}
