package br.com.acandido.core.status;

import org.eclipse.core.runtime.IStatus;

/**
 * @author Alan C�ndido <brodao@gmail.com>
 *
 */
public class ACInfoStatus extends ACStatus {

	/**
	 * Gera status de informa��o.
	 * 
	 * @param pluginId
	 * @param information
	 */
	public ACInfoStatus(String pluginId, String information, Object... args) {
		super(pluginId, IStatus.INFO, 0, format(information, args), null);
	}

}
