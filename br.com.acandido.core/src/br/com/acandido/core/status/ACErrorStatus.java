package br.com.acandido.core.status;

import org.eclipse.core.runtime.IStatus;

/**
 * @author Alan C�ndido <brodao@gmail.com>
 *
 */
public class ACErrorStatus extends ACStatus {

	/**
	 * Gera status de erro.
	 * 
	 * @param pluginId
	 * @param error
	 */
	public ACErrorStatus(String pluginId, Throwable throwable) {
		super(pluginId, IStatus.ERROR, 0, throwable.getMessage(), throwable);
	}

	public ACErrorStatus(String pluginId, String error, Object... args) {
		super(pluginId, IStatus.ERROR, 0, format(error, args), null);
	}

}
