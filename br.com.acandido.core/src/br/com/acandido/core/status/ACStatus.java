package br.com.acandido.core.status;

import org.eclipse.core.runtime.Status;

/**
 * @author Alan C�ndido <brodao@gmail.com>
 *
 */
public class ACStatus extends Status {

	/**
	 * Gera status.
	 * 
	 * @param pluginId
	 * @param level
	 * @param code
	 * @param message
	 * @param throwable
	 */
	public ACStatus(String pluginId, int level, int code, String message, Throwable throwable) {
		super(level, pluginId, code, message, throwable);
	}

	/**
	 * Formata texto com argumentos.
	 * 
	 * @param format
	 * @param args
	 * @return The formated text
	 * @see String#format(String, Object...)
	 */
	protected static String format(String format, Object[] args) {
		if (args.length == 0) {
			return format;
		}

		return String.format(format, args);
	}

}
