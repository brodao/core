package br.com.acandido.core.status;

import org.eclipse.core.runtime.IStatus;

/**
 * @author Alan C�ndido <brodao@gmail.com>
 *
 */
public class ACCancelStatus extends ACStatus {

	/**
	 * Gera status de erro.
	 * 
	 * @param pluginId
	 * @param reason
	 */
	public ACCancelStatus(String pluginId, String reason, Object... args) {
		super(pluginId, IStatus.CANCEL, 0, format(reason, args), null);
	}

}
