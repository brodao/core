package br.com.acandido.core;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.Version;

public final class ACUtils {

	private static final String TRACO = "----------------------------------------------------------------------------------\n";;

	private ACUtils() {
		// TODO Auto-generated constructor stub
	}

	public static StringBuffer getDeveloperInfo(Version coreVersion) {
		StringBuffer sb = new StringBuffer();

		sb.append("/====================v===========================================================\\\n");
		sb.append("|     /////// ////// | AC FERRAMENTAS - Adicionais para Eclipse e TDS            |\n");
		sb.append("|    //   // //      | (C) 2013-2015 Alan C�ndido (BR�D�O) <brodao@gmail.com>    |\n");
		sb.append(String.format("|   /////// //       | Vers�o n�cleo: %-42s |\n", coreVersion.toString()));
		sb.append("|  //   // //        | Visite: http://actools.sourceforge.net                    |\n");
		sb.append("| //   // //////     |         http://actoolsadvpl.sourceforge.net               |\n");
		sb.append("\\====================^===========================================================/\n");
		sb.append(".[====].[====].(----).(----).\\____/.[____].]@|--Y\\_\n");
		sb.append("__o__o___o__o___o__o___o__o___o___o___o__o___O___oo_\\____________________________\n");

		return sb;
	}

	public static StringBuffer getFooterInfo(String subtitle) {
		StringBuffer sb = new StringBuffer();

		if (subtitle != null) {
			sb.append(subtitle);
			sb.append("\n");
		}

		sb.append("/================================================================================\\\n");
		sb.append("| AC FERRAMENTAS (C) 2013-2015 Alan C�ndido (BR�D�O) <brodao@gmail.com>          |\n");
		sb.append("| Visite: http://actools.sourceforge.net e http://actoolsadvpl.sourceforge.net   |\n");
		sb.append("\\================================================================================/\n");

		return sb;
	}

	public static StringBuffer getHappyBirthday() {
		StringBuffer sb = new StringBuffer();

		sb.append("       	)         *             !  !  !  !  !  !     (                   ) \n");
		sb.append("       	(          (    *       |V||V||V||V||V||V|     )       )         ( \n");
		sb.append("       	 )   *      )           | || || || || || |    (       (   *       )\n");
		sb.append("       	(          (           (*******************)  *       *    )      *\n");
		sb.append("       	 )          )  *       (    |         |    )              (        \n");
		sb.append("       	(     (    (       !   (    *         *    )   !           )    (  \n");
		sb.append("       	 )   * )    )     |V|  (   \\|/       \\|/   )  |V|    *    (      ) \n");
		sb.append("       	(     (     *     | |  (<<<<<<<<<*>>>>>>>>>)  | |          )    (  \n");
		sb.append("       	 )     )        ((*******************************))       (  *   ) \n");
		sb.append("       	(     (   *     ((   VIDA  LONGA  E  PR�PSPERA   ))        )    (  \n");
		sb.append("       	 ) *   )     !  ((   *    *   *    *    *    *   ))  !    (      ) \n");
		sb.append("       	(     (     |V| ((  \\|/  \\|/ \\|/  \\|/  \\|/  \\|/  )) |V|    )    (  \n");
		sb.append("       	*)     )    | | ((^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^)) | |   (      ) \n");
		sb.append("       	(     (  (***********************************************) )  * (  \n");
		sb.append("       	 )     ) (*      |   |   __    ___    ___               *)(      ) \n");
		sb.append("       	(     (  (*      |   |  /  \\  |   \\  |   \\    \\  /      *) )    (  \n");
		sb.append("       	*      ) (*      |---| |----| |___/  |___/     \\/       *)(      ) \n");
		sb.append("       	      (  (*  *   |   | |    | |      |         /   *    *) )    (  \n");
		sb.append("       	       ) (* \\|/  |   | |    | |      |        /   \\|/   *) *     ) \n");
		sb.append("       	      (  (*                                             *)      (  \n");
		sb.append("       	 )  *  ) (*}+{}+{}+{}+{}+{}+{}+{}+{}+{}+{}+{}+{}+{}+{}+{*)       ) \n");
		sb.append("       	(     (  (*}-{}-{}-{}-{}-{}-{}-{}-{}-{}-{}-{}-{}-{}-{}-{*)       * \n");
		sb.append("       	 ) ((********************************************************))    \n");
		sb.append("       	(  ((}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*))    \n");
		sb.append("       	 ) ((      __                                                ))    \n");
		sb.append("       	(  ((     |  \\ _____  ___  _____        ___     __           ))    \n");
		sb.append("       	 ) ((     |__/   |   |   \\   |   |   | |   \\   /  \\   \\  /   ))    \n");
		sb.append("       	(  ((     |  \\   |   |___/   |   |---| |    > |----|   \\/    ))    \n");
		sb.append("       	*  ((  *  |__/ __|__ |  \\    |   |   | |___/  |    |   /  *  ))    \n");
		sb.append("       	   (( \\|/                                                \\|/ ))    \n");
		sb.append("       	   ((}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*{}*))    \n");
		sb.append("       	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ \n");

		return sb;
	}

	public static void writeFooter(final StringBuffer sb, final String symbolicName, final String version) {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");

		sb.append(TRACO);
		sb.append(String.format("Gerado em %s por %s vers�o %s\n", df.format(Calendar.getInstance().getTimeInMillis()),
				symbolicName, version));
	}

	public static void writeFooter(final StringBuffer sb) {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");

		sb.append(TRACO);
		sb.append(String.format("Gerado em %s\n", df.format(Calendar.getInstance().getTimeInMillis())));

	}

	public static long fileSize(final File file) throws IOException {
		Path path = FileSystems.getDefault().getPath(file.getParent(), file.getName());
		long size = 0;
		size = Files.size(path);
		return size;
	}

	@SuppressWarnings("unchecked")
	public static Object[] toArray(final String arrayString, final Class<? extends Object> classTarget)
			throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {

		if (!arrayString.isEmpty()) {
			@SuppressWarnings("rawtypes")
			ArrayList list = new ArrayList();
			String[] items = arrayString.substring(1, arrayString.length() - 1).split(",");

			Method method = classTarget.getDeclaredMethod("fromString", new Class[]{String.class});

			for (int i = 0; i < items.length; i++) {
				Object[] paramsObj = {items[i]};
				list.add(method.invoke(classTarget.newInstance(), paramsObj));
			}

			return list.toArray();
		}

		return new Object[0];
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static List loadFileSetExpression(final String fileSetExpression, final Class classRef) {
		ArrayList items = new ArrayList();

		try {
			Object[] aux = ACUtils.toArray(fileSetExpression, classRef);

			items.clear();
			for (Object element : aux) {
				items.add(element);
			}

		} catch (InstantiationException | IllegalAccessException | NoSuchMethodException | SecurityException
				| IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}

		return items;
	}

	/**
	 * Contatena array de strings, usando separador entre os itens.
	 * 
	 * @param strs
	 * @param separator
	 * @return
	 */
	public static String join(String[] strs, String separator) {
		StringBuilder sb = new StringBuilder();

		for (String str : strs) {
			sb.append(str);
			sb.append(separator);
		}
		String result = sb.toString();

		return result.substring(0, result.length() - separator.length());
	}

	public static IWorkbenchPage getActivePage() {
		IWorkbenchWindow activeWorkbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (activeWorkbenchWindow == null) {
			return null;
		}
		return activeWorkbenchWindow.getActivePage();
	}

	public static IEditorPart getActiveEditor() {
		IWorkbenchPage page = getActivePage();
		if (page == null) {
			return null;
		}

		return page.getActiveEditor();
	}

}
