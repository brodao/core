package br.com.acandido.core.logger;

import java.io.PrintStream;

import org.eclipse.core.runtime.ILog;
import org.eclipse.ui.console.MessageConsole;

/**
 * @author Alan C�ndido <brodao@gmail.com>
 *
 */
public class LoggerInfo {

	private PrintStream outFile;
	private PrintStream errFile;
	private final MessageConsole console;
	private ILog log;

	/**
	 * Construtor padr�o.
	 * 
	 * @param console
	 */
	public LoggerInfo(MessageConsole console) {
		this.console = console;
	}

	/**
	 * @return the outFile
	 */
	public PrintStream getOutFile() {
		return outFile;
	}

	/**
	 * @param outFile
	 *          the outFile to set
	 */
	public void setOutFile(PrintStream outFile) {
		this.outFile = outFile;
	}

	/**
	 * @return the errFile
	 */
	public PrintStream getErrFile() {
		return errFile;
	}

	/**
	 * @param errFile
	 *          the errFile to set
	 */
	public void setErrFile(PrintStream errFile) {
		this.errFile = errFile;
	}

	/**
	 * @return the log
	 */
	public ILog getLog() {
		return log;
	}

	/**
	 * @param log
	 *          the log to set
	 */
	public void setLog(ILog log) {
		this.log = log;
	}

	/**
	 * @return the console
	 */
	public MessageConsole getConsole() {
		return console;
	}

}
