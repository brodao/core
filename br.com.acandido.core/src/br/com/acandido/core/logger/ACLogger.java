package br.com.acandido.core.logger;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.swt.graphics.Color;
import org.eclipse.ui.console.MessageConsole;

/**
 * AC Logger
 * 
 */
public class ACLogger {

	protected static final Color ERROR_COLOR = new Color(null, 255, 0, 47);

	private final static Map<String, LoggerInfo> loggers = new HashMap<String, LoggerInfo>();

	private static final LoggerInfo defaultLoggerInfo = new LoggerInfo(ConsoleFactory.getConsole());

	public static void setLogFile(String pluginId, PrintStream out, PrintStream err) {
		LoggerInfo li = ACLogger.loggers.get(pluginId);

		li.setOutFile(out);
		li.setErrFile(err);
	}

	public static ILog getLogger(String pluginId) {
		LoggerInfo li = ACLogger.loggers.get(pluginId);

		if (li != null) {
			return li.getLog();
		}

		return null;
	}

	public static void unconfigureLogger(String pluginId) {
		LoggerInfo li = ACLogger.loggers.get(pluginId);

		if (li != null) {
			// TODO: destruir log?
			ACLogger.loggers.remove(pluginId);
		}
	}

	public static void configureLogger(String pluginId, ILog logger) {
		configureLogger(pluginId, logger, ConsoleFactory.getConsole());
	}

	public static void configureLogger(String pluginId, ILog logger, MessageConsole console) {
		LoggerInfo li = ACLogger.loggers.get(pluginId);

		if (li == null) {
			li = new LoggerInfo(console);
			ACLogger.loggers.put(pluginId, li);
		}

		if (logger != null) {
			li.setLog(logger);
		} else {
			ACLogger.loggers.remove(pluginId);
		}
	}

	public static void log(IStatus status) {
		ACLogger.loggers.get(status.getPlugin()).getLog().log(status);
	}

	public static LoggerInfo getLoggerInfo(String pluginId) {
		LoggerInfo li = ACLogger.loggers.get(pluginId);

		if (li == null) {
			li = ACLogger.defaultLoggerInfo;
		}

		return li;
	}

}