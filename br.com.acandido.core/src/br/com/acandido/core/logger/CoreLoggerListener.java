package br.com.acandido.core.logger;

import java.io.PrintStream;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.ui.console.MessageConsoleStream;
import org.eclipse.ui.statushandlers.StatusAdapter;
import org.eclipse.ui.statushandlers.StatusManager.INotificationListener;

import br.com.acandido.core.ACUtils;

public class CoreLoggerListener implements INotificationListener {

	/**
	 * Processa o status, efetuando o log.
	 * 
	 * @param status
	 * @param loggerInfo
	 */
	private void logging(IStatus status, LoggerInfo loggerInfo) {

		if (status.isMultiStatus()) {
			for (IStatus child : status.getChildren()) {
				logging(child, loggerInfo);
			}
		} else {
			MessageConsoleStream ms = loggerInfo.getConsole().newMessageStream();
			writeMessage(ms, status);

			if (loggerInfo.getOutFile() != null) {
				writeMessage(loggerInfo.getOutFile(), loggerInfo.getErrFile(), status, true);
			}
		}
	}

	private void writeMessage(MessageConsoleStream out, IStatus status) {
		switch (status.getSeverity()) {
			case IStatus.OK :
				break;
			case IStatus.INFO :
				out.setColor(ConsoleFactory.COLOR_INFORMATION);
				break;
			case IStatus.WARNING :
				out.setColor(ConsoleFactory.COLOR_WARNING);
				break;
			case IStatus.ERROR :
				out.setColor(ConsoleFactory.COLOR_ERROR);
				break;
			case IStatus.CANCEL :
				out.setColor(ConsoleFactory.COLOR_ERROR);
				break;
			default :
				break;
		}

		writeMessage(new PrintStream(out), new PrintStream(out), status, false);
	}

	private void writeMessage(PrintStream out, PrintStream err, IStatus status, boolean labeled) {
		PrintStream ps = out;
		Throwable throwable = null;
		String[] format = {"%s\n%15s%s", "%-15s%s", "[%04d] %s", "%-4s: %s", "%-4s: %s", "%5s %s"};

		if (err == null) {
			err = out;
		}

		if (status.getCode() == -1) {
			ps.println(String.format("%s", status.getMessage()));
		} else {
			String label = "";
			switch (status.getSeverity()) {
				case IStatus.OK :
					label = "OK";
					break;

				case IStatus.INFO :
					label = "INFO";
					break;
				case IStatus.WARNING :
					label = "WARN";
					break;
				case IStatus.ERROR :
					label = "ERRO";
					ps = err;
					throwable = status.getException();
					if (throwable != null) {
						if (throwable.getCause() == null) {
							throwable = null;
						}
					}
					break;
				case IStatus.CANCEL :
					label = "CANC";
					ps = System.err;
					break;
				default :
					break;
			}

			if (!labeled) {
				label = "";
				format[3] = "%s %s";
				format[4] = "%s %s";
				format[5] = "%s %s";
			}

			String[] messages = status.getMessage().split("\n");
			for (int i = 0; i < messages.length; i++) {
				String[] aux = messages[i].split("\t");
				if (aux.length > 1) {
					if (aux[0].length() > 15) {
						messages[i] = String.format(format[0], aux[0], "", aux[1]);
					} else {
						messages[i] = String.format(format[1], aux[0], aux[1]);
					}
				}
			}

			messages = ACUtils.join(messages, "\n").split("\n");

			if (status.getCode() != 0)
				messages[0] = String.format(format[2], status.getCode(), messages[0]);
			ps.println(String.format(format[3], label, messages[0]));
			for (int i = 1; i < messages.length; i++) {
				ps.println(String.format(format[5], " ", messages[i]));
			}

			if (throwable != null) {
				throwable.printStackTrace(System.err);
			}

			if ((status.getException() != null) && (status.getException().getCause() != null)) {
				status.getException().getCause().printStackTrace(ps);
			}
		}
	}

	@Override
	synchronized public void statusManagerNotified(int type, StatusAdapter[] adapters) {
		for (StatusAdapter statusAdapter : adapters) {
			String pluginId = statusAdapter.getStatus().getPlugin();
			LoggerInfo li = ACLogger.getLoggerInfo(pluginId);

			logging(statusAdapter.getStatus(), li);
		}

	}

}
