package br.com.acandido.core.logger;

import org.eclipse.swt.graphics.Color;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleFactory;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.osgi.framework.Version;

import br.com.acandido.core.ACUtils;
import br.com.acandido.core.CoreActivator;

/**

 */
public final class ConsoleFactory implements IConsoleFactory {

	private static final String CONSOLE_TITLE = "AC Ferramentas";

	public static final Color COLOR_INFORMATION = new Color(null, 72, 0, 255);
	public static final Color COLOR_ERROR = new Color(null, 255, 0, 47);
	public static final Color COLOR_WARNING = new Color(null, 86, 79, 185);

	private static MessageConsole console = null;

	@Override
	public void openConsole() {
		MessageConsole console = getConsole();
		if (console != null) {
			IConsoleManager manager = ConsolePlugin.getDefault().getConsoleManager();
			IConsole[] existing = manager.getConsoles();
			boolean exists = false;
			for (int i = 0; i < existing.length; i++) {
				if (console == existing[i])
					exists = true;
			}
			if (!exists)
				manager.addConsoles(new IConsole[]{console});
			manager.showConsoleView(console);
		}

	}

	public static MessageConsole getConsole() {
		if (console == null) {
			Version version = CoreActivator.getContext().getBundle().getVersion();
			console = new MessageConsole(CONSOLE_TITLE, null);
			MessageConsoleStream ms = console.newMessageStream();
			ms.println(ACUtils.getDeveloperInfo(version).toString());
		}

		return console;
	}

}
