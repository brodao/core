#!/bin/bash

sed -i~ "/<servers>/ a\
<server>\
  <id>accore-repo</id>\
  <username>${MAVEN_USERNAME}</username>\
  <password>${MAVEN_PASSWORD}</password>\
</server>" /usr/share/maven/conf/settings.xml

sed -i "/<profiles>/ a\
<profile>\
  <id>accore-repo</id>\
  <activation>\
    <activeByDefault>true</activeByDefault>\
  </activation>\
</profile>" /usr/share/maven/conf/settings.xml

